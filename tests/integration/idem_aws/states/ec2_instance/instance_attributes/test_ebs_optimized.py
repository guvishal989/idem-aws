import copy

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_update_ebs_optimized(hub, ctx, aws_ec2_instance, __test):
    # TODO this is not currently supported
    return
    state = copy.copy(aws_ec2_instance)
    state["ebs_optimized"] = True

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert ret["changes"]["new"]["ebs_optimized"] is True
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_ebs_optimized(hub, ctx, aws_ec2_instance, __test):
    # TODO this is not currently supported
    return
    state = copy.copy(aws_ec2_instance)
    state["ebs_optimized"] = False

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert ret["changes"]["new"]["ebs_optimized"] is False
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]
