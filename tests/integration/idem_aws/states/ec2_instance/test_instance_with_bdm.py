import tempfile
import time
from typing import Any
from typing import Dict

import pytest
import pytest_asyncio


@pytest_asyncio.fixture(scope="module", name="default_subnet")
async def get_default_subnet(hub, ctx, aws_ec2_default_subnets):
    subnet_id = next(iter(aws_ec2_default_subnets.values()))
    ret = await hub.exec.aws.ec2.subnet.get(ctx, resource_id=subnet_id, name=subnet_id)
    assert ret.result, ret.comment
    return ret.ret


@pytest_asyncio.fixture(scope="module", name="instance")
def persist_data():
    data = dict()
    yield data


@pytest_asyncio.fixture(scope="module", name="aws_ec2_volume")
async def override_aws_ec2_volume(hub, ctx, default_subnet) -> Dict[str, Any]:
    """
    Create and cleanup an EC2 volume for a module that needs it
    :return: a description of an EC2 volume
    """

    volume_temp_name = "idem-fixture-volume" + str(int(time.time()))
    ret = await hub.states.aws.ec2.volume.present(
        ctx,
        name=volume_temp_name,
        availability_zone=default_subnet["availability_zone"],
        size=1,
        tags={"Name": volume_temp_name},
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")
    resource_id = after.get("resource_id")

    for _ in range(10):
        # Wait until the volume is available
        get = await hub.exec.aws.ec2.volume.get(
            ctx,
            name=volume_temp_name,
            resource_id=resource_id,
        )
        if get.ret["state"] == "available":
            break
        else:
            await hub.pop.loop.sleep(10)
    yield after

    await hub.states.aws.ec2.volume.absent(
        ctx, name=volume_temp_name, resource_id=resource_id
    )


PRESENT_STATE = """
{name}:
  aws.ec2.instance.present:
    - name: {name}
    - instance_type: {instance_type}
    - image_id: {image_id}
    - running: True
    - block_device_mappings:
      - {bdm}
    - subnet_id: {subnet_id}
    - client_token: {name}
    - tags:
        Name: {name}
"""

# Parametrization options for running each test with --test first and then without --test
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
@pytest.mark.asyncio
async def test_present(
    hub,
    ctx,
    instance_name,
    aws_ec2_instance_type,
    aws_ec2_ami_pro,
    aws_ec2_volume,
    default_subnet,
    instance,
    idem_cli,
    __test,
):
    """
    Create a vanilla instance without using the instance fixture
    """
    bdm = dict(
        device_name="/dev/sda2",
        volume_id=aws_ec2_volume["resource_id"],
        delete_on_termination=False,
    )

    # Use idem_cli to run this state so that it reconciles properly
    state = PRESENT_STATE.format(
        name=instance_name,
        instance_type=aws_ec2_instance_type["resource_id"],
        image_id=aws_ec2_ami_pro["resource_id"],
        bdm=bdm,
        subnet_id=default_subnet["resource_id"],
    )
    with tempfile.NamedTemporaryFile(suffix=".sls") as fh:
        fh.write(state.encode())
        fh.flush()
        cmd = ["state", fh.name]
        if ctx.test:
            cmd.append("--test")

        cli_ret = idem_cli(*cmd)
        ret = cli_ret.json[
            f"aws.ec2.instance_|-{instance_name}_|-{instance_name}_|-present"
        ]

    if ctx.test:
        return

    # Wait for the resource to exist and be running
    resource = await hub.tool.boto3.resource.create(
        ctx, "ec2", "Instance", ret["new_state"]["resource_id"]
    )
    await hub.tool.boto3.resource.exec(resource, "wait_until_exists")
    instance["resource_id"] = ret["new_state"]["resource_id"]

    assert ret["result"], "\n".join(str(c) for c in ret["comment"])

    # Verify that we can perform a successful "get" and that the id matches the present new_state
    get = await hub.exec.aws.ec2.instance.get(ctx, resource_id=instance["resource_id"])
    assert get.result, get.comment
    assert get.ret, get.comment
    assert get.ret["resource_id"] == instance["resource_id"]
    assert len(get.ret["block_device_mappings"]) == 2
    for bd in get.ret["block_device_mappings"]:
        if bd["volume_id"] == aws_ec2_volume["resource_id"]:
            assert bd == bdm
            break
    else:
        assert False, "Volume not found block device mappings"


@pytest.mark.localstack(False)
@pytest.mark.asyncio
async def test_change_volume(
    hub,
    ctx,
    instance_name,
    aws_ec2_instance_type,
    aws_ec2_ami_pro,
    default_subnet,
    aws_ec2_volume,
    instance,
    __test,
):
    bdm = dict(
        device_name="/dev/sda2",
        volume_id=aws_ec2_volume["resource_id"],
        delete_on_termination=True,
    )
    present_kwargs = dict(
        name=instance_name,
        resource_id=instance["resource_id"],
        instance_type=aws_ec2_instance_type["resource_id"],
        image_id=aws_ec2_ami_pro["resource_id"],
        running=True,
        block_device_mappings=[bdm],
        subnet_id=default_subnet["resource_id"],
        client_token=instance_name,
        tags={"Name": instance_name},
    )
    ret = await hub.states.aws.ec2.instance.present(ctx, **present_kwargs)
    assert ret["result"], "\n".join(str(c) for c in ret["comment"])

    if ctx.test:
        return

    # Verify that we can perform a successful "get" and that the id matches the present new_state
    get = await hub.exec.aws.ec2.instance.get(ctx, resource_id=instance["resource_id"])
    assert get.result, get.comment
    assert get.ret, get.comment
    assert get.ret["resource_id"] == instance["resource_id"]
    assert len(get.ret["block_device_mappings"]) == 2
    for bd in get.ret["block_device_mappings"]:
        if bd["volume_id"] == aws_ec2_volume["resource_id"]:
            assert bd == bdm
            break
    else:
        assert False, "Volume not found block device mappings"


@pytest.mark.localstack(False)
@pytest.mark.asyncio
async def test_remove_volume(
    hub,
    ctx,
    instance_name,
    aws_ec2_instance_type,
    aws_ec2_ami_pro,
    default_subnet,
    aws_ec2_volume,
    instance,
    __test,
):
    present_kwargs = dict(
        name=instance_name,
        instance_type=aws_ec2_instance_type["resource_id"],
        image_id=aws_ec2_ami_pro["resource_id"],
        running=True,
        block_device_mappings=[],
        subnet_id=default_subnet["resource_id"],
        client_token=instance_name,
        tags={"Name": instance_name},
    )
    ret = await hub.states.aws.ec2.instance.present(ctx, **present_kwargs)

    assert ret["result"], "\n".join(str(c) for c in ret["comment"])

    if ctx.test:
        return


@pytest.mark.localstack(False)
@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name, instance):
    """
    Destroy the instance created by the present state
    """
    ret = await hub.states.aws.ec2.instance.absent(
        ctx, name=instance_name, resource_id=instance["resource_id"]
    )
    assert ret["result"], ret["comment"]
