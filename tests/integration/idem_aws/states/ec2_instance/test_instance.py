"""
Test simple creation and deletion of an instance
"""
import pytest


# Parametrization options for running each test with --test first and then without --test
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="present")
@pytest.mark.asyncio
async def test_present(
    hub, ctx, instance_name, aws_ec2_instance_type, aws_ec2_ami_pro, __test, instance
):
    """
    Create a vanilla instance without using the instance fixture
    """
    ret = await hub.states.aws.ec2.instance.present(
        ctx,
        name=instance_name,
        # Don't specify a subnet_id to use the default subnet
        # subnet_id=aws_ec2_subnet.get("SubnetId"),
        instance_type=aws_ec2_instance_type["resource_id"],
        image_id=aws_ec2_ami_pro["resource_id"],
        client_token=instance_name,
        tags={"Name": instance_name},
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"], ret["comment"]

    if ctx.test:
        return

    verify_instance_has_basic_networking_details(ret["new_state"])

    # Wait for the resource to exist and be running
    resource = await hub.tool.boto3.resource.create(
        ctx, "ec2", "Instance", ret["new_state"]["resource_id"]
    )
    await hub.tool.boto3.resource.exec(resource, "wait_until_exists")
    await hub.tool.boto3.resource.exec(resource, "wait_until_running")

    instance["resource_id"] = ret["new_state"]["resource_id"]

    # Verify that we can perform a successful "get" and that the id matches the present new_state
    get = await hub.exec.aws.ec2.instance.get(
        ctx, resource_id=ret["new_state"]["resource_id"]
    )
    assert get.result, get.comment
    assert get.ret, get.comment
    assert get.ret["resource_id"] == ret["new_state"]["resource_id"]


@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_get(hub, ctx, instance_name):
    """
    Verify that "get" is successful after an instances has been created
    """
    get = await hub.exec.aws.ec2.instance.get(
        ctx, filters=[{"Name": "tag:Name", "Values": [instance_name]}]
    )
    assert get.result, get.comment
    assert get.ret, get.comment
    verify_instance_has_basic_networking_details(get.ret)

    ret = await hub.exec.aws.ec2.instance.get(ctx, resource_id=get.ret.resource_id)
    assert ret.result, ret.comment
    assert ret.ret, ret.comment

    # Verify that the instance id matches for both
    assert ret.ret["resource_id"] == get.ret["resource_id"]


@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_list(hub, ctx, instance_name, instance):
    """
    Verify that "list" is successful after an instances has been created
    """

    ret = await hub.exec.aws.ec2.instance.list(
        ctx, filters=[{"Name": "instance-id", "Values": [instance["resource_id"]]}]
    )
    assert ret.result, ret.comment
    assert ret.ret, ret.comment
    # Verify that the created instance is in the list
    assert ret.ret[0]["resource_id"] == instance["resource_id"]
    verify_instance_has_basic_networking_details(ret.ret[0])


@pytest.mark.localstack(False)
@pytest.mark.asyncio
async def test_fixture(hub, ctx, aws_ec2_instance, instance_name, instance):
    """
    Use the instance fixture and verify that it is functional.
    Nothing new should be created by the fixture in this test since both the fixture
    and the instance present state tests use the "instance_name" module level fixture
    """
    # Verify that the fixture resulted in a usable instance
    assert aws_ec2_instance["resource_id"] == instance["resource_id"]


@pytest.mark.asyncio
@pytest.mark.localstack(
    False, "Localstack Bug: https://github.com/localstack/localstack/issues/6076"
)
async def test_describe(hub, ctx, instance_name, aws_ec2_instance, __test, instance):
    """
    Describe all instances and run the "present" state the described instance created for this module.
    No changes should be made and present/search/describe should have equivalent parameters.
    """
    # Describe all instances
    ret = await hub.states.aws.ec2.instance.describe(ctx)
    assert instance["resource_id"] in ret

    # Run the present state for our resource created by describe
    instance_kwargs = {}
    for pair in ret[instance["resource_id"]]["aws.ec2.instance.present"]:
        instance_kwargs.update(pair)

    # Run the present state on the result of "describe, no changes should be made
    instance_ret = await hub.states.aws.ec2.instance.present(ctx, **instance_kwargs)

    assert instance_ret["result"], instance_ret["comment"]

    # No changes should have been made!
    # We just created this state from describe
    assert instance_ret["old_state"] == instance_ret["new_state"]
    verify_instance_has_basic_networking_details(instance_ret["new_state"])
    assert not instance_ret["changes"]


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
@pytest.mark.dependency(depends=["present"])
async def test_stop(hub, ctx, instance_name, instance):
    ret = await hub.exec.aws.ec2.instance.stop(ctx, instance_id=instance["resource_id"])
    assert ret.result, ret.comment


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
@pytest.mark.dependency(depends=["present"])
async def test_already_stopped(hub, ctx, instance_name, instance):
    ret = await hub.exec.aws.ec2.instance.stop(ctx, instance_id=instance["resource_id"])
    assert ret.result, ret.comment


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
@pytest.mark.dependency(depends=["present"])
async def test_start(hub, ctx, instance_name, instance):
    ret = await hub.exec.aws.ec2.instance.start(
        ctx, instance_id=instance["resource_id"]
    )
    assert ret.result, ret.comment


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
@pytest.mark.dependency(depends=["present"])
async def test_already_started(hub, ctx, instance_name, instance):
    ret = await hub.exec.aws.ec2.instance.start(
        ctx, instance_id=instance["resource_id"]
    )
    assert ret.result, ret.comment


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name, __test, instance):
    """
    Destroy the instance created by the present state
    """
    ret = await hub.states.aws.ec2.instance.absent(
        ctx, name=instance_name, resource_id=instance["resource_id"]
    )
    assert ret["result"], ret["comment"]

    if ctx.test:
        assert not ret["new_state"]
    else:
        assert ret["new_state"]
        # Wait until terminated so the subnet doesn't have any leftover dependencies
        resource = await hub.tool.boto3.resource.create(
            ctx, "ec2", "Instance", instance["resource_id"]
        )
        await hub.tool.boto3.resource.exec(resource, "wait_until_terminated")


def verify_instance_has_basic_networking_details(instance_state):
    assert instance_state["public_ip_address"], "Instance should have public IP address"
    assert instance_state[
        "network_interfaces"
    ], "Instance should have network interfaces"
    assert (
        len(instance_state["network_interfaces"]) == 1
    ), "Instance should have only one network interface"
    network_interface = instance_state["network_interfaces"][0]
    assert network_interface[
        "subnet_id"
    ], "Instance's network interface should have a subnet"
    assert network_interface[
        "public_ip_address"
    ], "Instance's network interface should have a public IP address"
    assert network_interface[
        "private_ip_address"
    ], "Instance's network interface should have a private IP address"
    assert network_interface[
        "mac_address"
    ], "Instance's network interface should have a MAC address"
