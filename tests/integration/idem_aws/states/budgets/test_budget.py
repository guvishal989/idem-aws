import copy
import time
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
NEW_TIME_PERIOD = {
    "End": "2023-07-15T00:00:00Z",
    "Start": "2022-06-01T00:00:00Z",
}
NEW_BUDGET_LIMIT = {"Amount": "10000.0", "Unit": "USD"}
NEW_TIME_UNIT = "QUARTERLY"
PARAMETER = {
    "name": "idem-test-budget-" + str(int(time.time())),
    "budget_name": "idem-test-budget-" + str(int(time.time())),
    "budget_limit": {"Amount": "1000.0", "Unit": "USD"},
    "cost_filters": {"Service": ["Amazon API Gateway", "AmazonCloudWatch"]},
    "cost_types": {
        "IncludeCredit": False,
        "IncludeDiscount": True,
        "IncludeOtherSubscription": True,
        "IncludeRecurring": True,
        "IncludeRefund": False,
        "IncludeSubscription": True,
        "IncludeSupport": True,
        "IncludeTax": True,
        "IncludeUpfront": True,
        "UseAmortized": False,
        "UseBlended": False,
    },
    "time_unit": "MONTHLY",
    "time_period": {
        "End": "2023-06-15T00:00:00Z",
        "Start": "2022-05-01T00:00:00Z",
    },
    "budget_type": "COST",
    "notifications_with_subscribers": [
        {
            "Notification": {
                "ComparisonOperator": "GREATER_THAN",
                "NotificationState": "OK",
                "NotificationType": "FORECASTED",
                "Threshold": 90.0,
            },
            "Subscribers": [
                {"Address": "ashishag@vmware.com", "SubscriptionType": "EMAIL"}
            ],
        }
    ],
}


@pytest.mark.asyncio
@pytest.mark.localstack(False, "Localstack does not support budget resource")
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test):
    global PARAMETER

    ctx["test"] = __test

    ret = await hub.states.aws.budgets.budget.present(ctx, **PARAMETER)

    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.budgets.budget",
                name=PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.budgets.budget",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]

    assert_budget(hub, ctx, resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False, "Localstack does not support budget resource")
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.budgets.budget.describe(ctx)
    assert "aws.budgets.budget.present" in describe_ret.get(PARAMETER["name"])
    describe_params = describe_ret.get(PARAMETER["name"]).get(
        "aws.budgets.budget.present"
    )
    described_resource_map = dict(ChainMap(*describe_params))
    assert_budget(hub, ctx, described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False, "Localstack does not support budget resource")
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="modify_budget_attributes", depends=["present"])
async def test_modify_attributes(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test

    new_parameter["time_period"] = NEW_TIME_PERIOD
    new_parameter["time_unit"] = NEW_TIME_UNIT
    new_parameter["budget_limit"] = NEW_BUDGET_LIMIT

    ret = await hub.states.aws.budgets.budget.present(ctx, **new_parameter)

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")

    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.budgets.budget",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.budgets.budget",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert_budget(hub, ctx, ret["old_state"], PARAMETER)
    assert_budget(hub, ctx, ret["new_state"], new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.localstack(False, "Localstack does not support budget resource")
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="test_exec_get", depends=["modify_budget_attributes"])
async def test_exec_get(hub, ctx, __test):
    global PARAMETER
    ret = await hub.exec.aws.budgets.budget.get(
        ctx=ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert_budget(hub, ctx, ret["ret"], PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False, "Localstack does not support budget resource")
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["test_exec_get"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.budgets.budget.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert_budget(hub, ctx, ret["old_state"], PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.budgets.budget",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.budgets.budget",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.localstack(False, "Localstack does not support budget resource")
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.budgets.budget.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.budgets.budget",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


def assert_budget(hub, ctx, resource, parameters):
    assert parameters["budget_name"] == resource.get("budget_name")
    assert parameters["time_unit"] == resource.get("time_unit")
    assert parameters["budget_type"] == resource.get("budget_type")
    assert parameters["budget_limit"] == resource.get("budget_limit")
    assert parameters["cost_filters"] == resource.get("cost_filters")
    assert parameters["time_period"] == resource.get("time_period")
    assert parameters["notifications_with_subscribers"] == resource.get(
        "notifications_with_subscribers"
    )
