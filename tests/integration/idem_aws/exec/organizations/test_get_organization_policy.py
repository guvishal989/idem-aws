import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_get(hub, ctx, aws_organization_policy):
    policy_name = "idem-test-policy-" + str(uuid.uuid4())
    ret = await hub.exec.aws.organizations.policy.get(
        ctx,
        name=policy_name,
        resource_id=aws_organization_policy["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_organization_policy["resource_id"] == resource.get("resource_id")
