ADD_TAG_FUNCTIONS = (
    "add_tags",
    "tag_resource",
    "create_tags",
    "add_tags_to_resource",
)

REMOVE_TAG_FUNCTIONS = (
    "remove_tags",
    "untag_resource",
    "delete_tags",
    "remove_tags_from_resource",
)

SINGLE_TAG_UPDATE_FUNCTIONS = (
    "change_tags_for_resource",
    "change_tags",
)

LIST_TAG_FUNCTIONS = (
    "list_tags",
    "get_tags",
    "describe_tags",
    "list_tags_for_resource",
)

NAME_PARAMETER = {
    "default": None,
    "doc": "An Idem name of the resource",
    "param_type": "str",
    "required": True,
    "target": "hardcoded",
    "target_type": "arg",
}

RESOURCE_ID_PARAMETER = {
    "default": None,
    "doc": "An identifier of the resource in the provider",
    "param_type": "str",
    "required": True,
    "target": "hardcoded",
    "target_type": "arg",
}

OLD_TAGS_PARAMETER = {
    "default": None,
    "doc": "Dict in the format of {tag-key: tag-value}",
    "param_type": "dict",
    "required": True,
    "target": "hardcoded",
    "target_type": "arg",
}

NEW_TAGS_PARAMETER = {
    "default": None,
    "doc": "Dict in the format of {tag-key: tag-value}",
    "param_type": "dict",
    "required": True,
    "target": "hardcoded",
    "target_type": "arg",
}

GET_TAGS_FOR_RESOURCE_REQUEST = """
    result = dict(comment=[], result=True, ret=None)

    if not resource_id:
        result["result"] = False
        result["comment"] = ["resource_id parameter is None"]
        return result

    # TODO: Map resource_id to respective input param
    tags_ret = await hub.exec.boto3.client.{{ function.hardcoded.aws_service_name }}.{{ function.hardcoded.boto3_function }}(
        ctx,
        **{{ parameter.mapping.kwargs|default({}) }}
    )

    if not tags_ret["result"]:
        result["result"] = False
        result["comment"] = tags_ret["comment"]
        return result


    result["ret"] = hub.tool.aws.tag_utils.convert_tag_list_to_dict(
        tags_ret.get("ret").get("{{ function.hardcoded.response_key }}") if tags_ret.get("result") else None
    )

    return result
"""

UPDATE_TAGS_REQUEST = """
    result = dict(comment=[], result=True, ret=None)

    tags_to_add = {}
    tags_to_remove = {}
    if new_tags is not None:
        tags_to_remove, tags_to_add = hub.tool.aws.tag_utils.diff_tags_dict(
            old_tags=old_tags, new_tags=new_tags
        )

    if (not tags_to_remove) and (not tags_to_add):
        # If there is nothing to add or remove, return from here with old tags, if present
        result["ret"] = copy.deepcopy(old_tags if old_tags else {})
        return result

    {% if function.hardcoded.single_update %}
        # TODO: Map inputs key to respective input boto3 function params
        # Input params: {{ function.hardcoded.update_tags_input_params }}
        change_tags_ret = await hub.exec.boto3.client.{{ function.hardcoded.aws_service_name }}.{{ function.hardcoded.update_tags_boto3_function }}(
            ctx,
            ResourceType=resource_type,
            ResourceId=resource_id,
            AddTags=hub.tool.aws.tag_utils.convert_tag_dict_to_list(tags_to_add),
            RemoveTagKeys=list(tags_to_remove) if tags_to_remove else None,
        )

        if not change_tags_ret["result"]:
            result["comment"] = change_tags_ret["comment"]
            result["result"] = False
            return result
    {% else %}
    if tags_to_remove:
        if not ctx.get("test", False):
            # TODO: Map untag_resource input params to target values
            # Input params: {{ function.hardcoded.untag_resource_input_params }}
            delete_ret = await {{ function.hardcoded.untag_resource_boto3_function }}(
                ctx,
                ResourceId=resource_id,
                TagKeys=list(tags_to_remove.keys()),
            )
            if not delete_ret["result"]:
                result["comment"] = delete_ret["comment"]
                result["result"] = False
                return result

    if tags_to_add:
        if not ctx.get("test", False):
            # TODO: Map untag_resource input params to target values
            # Input params: {{ function.hardcoded.tag_resource_input_params }}
            add_ret = await {{ function.hardcoded.tag_resource_boto3_function }}(
                ctx,
                ResourceId=resource_id,
                Tags=hub.tool.aws.tag_utils.convert_tag_dict_to_list(tags_to_add),
            )
            if not add_ret["result"]:
                result["comment"] += add_ret["comment"]
                result["result"] = False
                return result

    result["ret"] = new_tags
    {% endif %}

    if ctx.get("test", False):
        result["comment"] = hub.tool.aws.comment_utils.would_update_tags_comment(
            tags_to_remove=tags_to_remove, tags_to_add=tags_to_add
        )
    else:
        result["comment"] = hub.tool.aws.comment_utils.update_tags_comment(
            tags_to_remove=tags_to_remove, tags_to_add=tags_to_add
        )
    return result
"""
